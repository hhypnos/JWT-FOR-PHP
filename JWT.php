<?php

/*
 * Copyright(c)2018 HYPNOS
 */

class JWT {
    private $alg;
    private $hash;
    private $data;

    private function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    private function base64url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    public function encode($header, $payload, $key) {
        $this->data = $this->base64url_encode($header) . '.' . $this->base64url_encode($payload);
        return $this->data.'.'.$this->JW($header, $key);
    }

    public function decode($token, $key) {
        list($header, $payload, $signature) = explode('.', $token);
        $this->data = $header . '.' . $payload;
        if ($signature == $this->JW($this->base64url_decode($header), $key)) {
            return $this->base64url_decode($payload);
        }
        exit('Invalid Signature');
    }

    private function encryptData($algorithm)
     {

        switch ($algorithm) {

            case "plaintext":
                $this->alg = 'plaintext';
                break;
            case "HS256":
                $hash = 'sha256';
                break;
            case "HS384":
                $hash = 'sha384';
                break;
            case "HS512":
                $hash = 'sha512';
                break;
        }
        if($algorithm != "plaintext"){
          if (in_array($hash, hash_algos())){
            $this->hash = $hash;
          }
        }
    }

    private function JW($header, $key)
    {
        $json = json_decode($header);
        $this->encryptData($json->alg);
        if ($this->alg == 'plaintext') {
            return '';
        }
        return $this->base64url_encode(hash_hmac($this->hash, $this->data, $key, true));
    }
}

?>
