<?php

require_once "JWT.php";
$header = '{
  "alg":"HS256",
  "typ":"JWT"
  }';
$payload = '{

  "auth":{
    "username":"58a9ff876dac49c3272a3f983f986c3e",
    "password":"c0367a0fad0d5de21d8b71b7e630d709"
  }
}';
$key = '58a9ff876dac49c3272a3f983f986c3e';
$JWT = new JWT;
$token = $JWT->encode($header, $payload, $key);
$json = $JWT->decode($token, $key);
echo 'Header: '.$header."\n\n";
echo 'Payload: '.$payload."\n\n";
echo 'HMAC Key: '.$key."\n\n";
echo 'JSON Web Token: '.$token."\n\n";
echo 'JWT Decoded: '.$json."\n\n";

?>
